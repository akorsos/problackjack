#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R13.1 Terms
Recursion - A method for computing a result by decomposing the inputs into simpler values and applying the same method
    to them.

Iteration - The repeated calling of a specific block of methods within a program.

Infinite Recursion - A set of instructions in a program that, when called, cause a cyclic repetition of the same
    instructions until the program is forcefully terminated.

Recursive Helper Method - A method that can call itself with simpler values. It must handle the simplest values
    without calling itself.

R13.2 Smallest value
Create a variable that keeps track of position with the array and instantiate it to 1
Create a variable that holds the min value and instantiate it to the value at array[0]

if(array[position] < min)
    min = array[position]
    position++
    recall method
return min

R13.3 Sort array of numbers
First find the smallest number in the array with the above algorithm and place that number at array[0]
Iterate position to the number at array[1], set that number to the min, rerun the above algorithm
Set the min to array[1]
Iterate again and rerun the algorithm until the you reach array.length(), the stopping condition

R13.6 Exponents recursively
exp(x,n)
if(n == 0)
    return 1
else
    return x * exp(x,n-1)

R13.8 Factorial recursively
factorial(x,n)
if(n == 1)
    return 1
else
    return x * factorial(x,n-1)!







