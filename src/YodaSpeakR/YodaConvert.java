package YodaSpeakR;

import java.util.*;

public class YodaConvert {

    public String yodaSpeak(String input){
        List<String> sentence = Arrays.asList(input.split(" "));
        return reverse(sentence, sentence.size()-1);
    }

    String output = "";
    public String reverse(List sentence, int end){
        if(end >= 0){
            output += sentence.get(end) + " ";
            end--;
            reverse(sentence, end);
        }
        return output;
    }
}
