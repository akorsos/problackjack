package YodaSpeakI;

import java.util.*;

public class YodaConvert {

    public String yodaSpeak(String input){
        List<String> sentence = Arrays.asList(input.split(" "));
        String output = "";
        for(int i = sentence.size()-1; i >= 0; i--){
            output += sentence.get(i) + " ";
        }
        return output;
    }
}
