package YodaSpeakI;

import java.util.Scanner;

public class UI {
    public static void main(String[] args) {

        String input;
        Scanner scan = new Scanner(System.in);

        System.out.println("Welcome to Yoda Speak!");
        System.out.println("Enter a sentence you'd like to convert: ");
        input = scan.nextLine();

        YodaConvert y1 = new YodaConvert();
        System.out.println(y1.yodaSpeak(input));
    }
}
